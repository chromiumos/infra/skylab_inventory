# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""This module provides functions to manage servers in skylab inventory."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from skylab_inventory import translation_utils
from skylab_inventory.protos import common_pb2
from skylab_inventory.protos import lab_pb2
from skylab_inventory.protos import server_pb2

from google.protobuf.json_format import MessageToJson
# pylint: disable=cros-logging-import
import logging


class SkylabServerActionError(Exception):
  """Exception raised when action on skylab server failed."""


def _verify_server(servers, hostname, environment, exists=True):
  """Verify the existence of the server.

  Args:
    servers: A list of servers currently in the inventory.
    hostname: The hostname of the server to verify.
    environment: Environment of the server to verify.
    exists: If true, a server with given |hostname| and |environment| must
            exist, else, no server can exist with |hostname|.

  Returns:
    The server object if found in the servers list.

  Raises:
    SkylabServerActionError: If failed to verify the server.
  """
  server = None
  for s in servers:
    if s.hostname == hostname:
      server = s
      break

  if exists:
    if (server is None or
        server.environment != translation_utils.ENVIRONMENT_MAP[environment]):
      raise SkylabServerActionError(
          'Server %s with environment %s does not exist.' %
          (hostname, environment))
  else:
    if server is not None:
      raise SkylabServerActionError(
          'Server %s already exists in environment %s.' %
          (hostname, common_pb2.Environment.Name(server.environment)))

  return server


def _add_role(server, role, resolve_mapping=True):
  """Add a role to the server.

  Args:
    server: An object of server_pb2.Server.
    role: Role to be added to the server.
    resolve_mapping: True if the role needs to be resolved by
                     translation_utils.ROLE_MAP.

  Raises:
    SkylabServerActionError: If failed to add the role.
  """
  if resolve_mapping:
    role_value = translation_utils.ROLE_MAP[role]
  else:
    role_value = role

  if role_value in server.roles:
    raise SkylabServerActionError(
        'Server %s already has role %s.' % (server.hostname, role))

  server.roles.append(role_value)

  logging.debug('Adding role %s to server %s.', role, server.hostname)


def _delete_role(server, role, resolve_mapping=True):
  """Delete a role from the server.

  Args:
    server: An object of server_pb2.Server.
    role: Role to be deleted from the server.
    resolve_mapping: True if the role needs to be resolved by
                     translation_utils.ROLE_MAP.

  Raises:
    SkylabServerActionError: If failed to delete the role.
  """
  if resolve_mapping:
    role_value = translation_utils.ROLE_MAP[role]
  else:
    role_value = role

  if role_value not in server.roles:
    raise SkylabServerActionError(
        'Server %s does not have role %s.' % (server.hostname, role))

  server.roles.remove(role_value)

  logging.debug('Deleting role %s is from server %s.', role, server.hostname)


def _change_status(server, status):
  """Change the status of the server.

  Args:
    server: An object of server_pb2.Server.
    status: New status of the server.

  Raises:
    SkylabServerActionError: If failed to change the status.
  """
  translation_utils.validate_server_status(status)
  status_value = translation_utils.STATUS_MAP[status]

  if server.status == status_value:
    raise SkylabServerActionError(
        'Server %s already has status of %s.' %
        (server.hostname, status))

  if (not server.roles and
      status_value == server_pb2.Server.STATUS_PRIMARY):
    raise SkylabServerActionError(
        'Server %s has no role associated. Server must have a role to '
        'be in status primary.' % server.hostname)

  # TODO(nxia):  Post a warning if the server's status will be changed from
  # primary to other value and the server is running a unique role. e.g.
  # scheduler.

  prev_status = server_pb2.Server.Status.Name(server.status)
  server.status = status_value
  cur_status = server_pb2.Server.Status.Name(server.status)

  logging.debug('Changing status of server %s from %s to %s.',
                server.hostname, prev_status, cur_status)


def _change_attribute(server, attribute, value):
  """Change the value of an attribute of the server.

  Args:
    server: An object of server_pb2.Server.
    attribute: Name of an attribute of the server.
    value: Value of the attribute of the server.

  Raises:
    SkylabServerActionError: If the attribute already exists and has the
                             given value.
  """
  translation_utils.validate_server_attribute(attribute)
  value = translation_utils.SERVER_ATTRIBUTE_TYPE_MAP[attribute](value)

  old_value = getattr(server.attributes, attribute)
  if old_value == value:
    raise SkylabServerActionError(
        'Attribute %s for server %s already has value of %s.' %
        (attribute, server.hostname, value))

  setattr(server.attributes, attribute, value)

  logging.debug('Changing attribute `%s` of server %s from %s to %s.',
                attribute, server.hostname, old_value, value)


def _delete_attribute(server, attribute):
  """Delete the attribute from the host.

  Args:
    server: An object of server_pb2.Server.
    attribute: Name of an attribute of the server.

  Raises:
    SkylabServerActionError: If the server doesn't have the attribute.
  """
  translation_utils.validate_server_attribute(attribute)

  if server.HasField('attributes') and server.attributes.HasField(attribute):
    server.attributes.ClearField(attribute)
  else:
    raise SkylabServerActionError(
        'Server %s does not have attribute `%s`' %
        (server.hostname, attribute))

  logging.debug('Deleting attribute `%s` from server %s.',
                attribute, server.hostname)


def add_dut_uids(server, devices):
  """add uids of the devices to the server.

  Args:
    server: An instance of server_pb2.Server to add the uids.
    devices: A list of device.Device instances to add to the server.
  """
  uids = [d.common.id for d in devices]
  server.dut_uids.extend(uids)


def remove_dut_uids(server, devices):
  """remove uids of the devices from the server.

  Args:
    server: An instance of server_pb2.Server to remove the uids.
    devices: A list of device.Device instances. Remove them from the server if
             the server contains the uids.
  """
  uids = [d.common.id for d in devices]
  for uid in uids:
    if uid in server.dut_uids:
      server.dut_uids.remove(uid)


def get_servers(infrastructure, environment, hostname=None, role=None,
                status=None):
  """Find servers with the given hostname, role and status.

  Args:
    infrastructure: An object of lab_pb2.Infrastructure loaded with
                    inventory data.
    environment: Only list servers of the given environment.
    hostname: Only list servers of the given hostname if it's not None.
    role: Only list servers of the given role if it's not None.
    status: Only list servers of the given status if it's not None.

  Returns:
    A list of server objects with given hostname, role and status.
  """
  server_list = []

  for s in infrastructure.servers:
    if s.environment != translation_utils.ENVIRONMENT_MAP[environment]:
      continue
    if hostname and s.hostname != hostname:
      continue
    if role and translation_utils.ROLE_MAP[role] not in s.roles:
      continue
    if status and s.status != translation_utils.STATUS_MAP[status]:
      continue

    server_list.append(s)

  return server_list


def format_servers_json(servers):
  """Convert servers to a json string.

  Args:
    servers: A list of server_pb2.Server instances.

  Returns:
    Json format of a lab_pb2.Infrastructure instance which contains the servers.
  """
  infra = lab_pb2.Infrastructure()
  infra.servers.extend(servers)
  return MessageToJson(infra)


def create(infrastructure, hostname, environment, role=None, note=None):
  """Create a new server.

  Args:
    infrastructure: An lab_pb2.Infrastructure object loaded with inventory data.
    hostname: hostname of the server.
    environment: The environment to create server.
    role: role of the new server, default to None.
    note: notes about the server, default to None.

  Returns:
    A Server object that contains the server information.
  """
  servers = infrastructure.servers
  _verify_server(servers, hostname, environment, exists=False)

  new_server = server_pb2.Server(
      hostname=hostname,
      notes=note,
      environment=translation_utils.ENVIRONMENT_MAP[environment],
      status=translation_utils.STATUS_MAP['primary'])

  if role:
    _add_role(new_server, role)

  servers.extend([new_server])

  logging.debug('Creating a new server %s.', hostname)

  return new_server


def delete(infrastructure, hostname, environment):
  """Delete a given server.

  Args:
    infrastructure: A lab_pb2.Infrastructure object loaded with inventory data.
    hostname: hostname of the server to be deleted.
    environment: environment of the server to be deleted.

  Raises:
    SkylabServerActionError: If failed to delete the server.
  """
  servers = infrastructure.servers
  target_server = _verify_server(servers, hostname, environment)
  if target_server.status == server_pb2.Server.STATUS_PRIMARY:
    for role in target_server.roles:
      _delete_role(target_server, role, resolve_mapping=False)

  servers.remove(target_server)

  logging.debug('Deleting server %s.', hostname)


def modify(infrastructure, hostname, environment, role=None, delete_role=False,
           status=None, note=None, attribute=None, value=None,
           delete_attribute=False):
  """Modify a given server.

  Args:
    infrastructure: An object of lab_pb2.Infrastructure loaded with
                    inventory data.
    hostname: hostname of the server to be modified.
    environment: environment of the server to be modified.
    role: Role to be modified.
    status: Modify server status.
    delete_role: Whether to delete the given role.
    note: Note of the server.
    attribute: Name of an attribute to be modified.
    value: Value of an attribute to be modified.
    delete_attribute: Whether to delete the given attribute.

  Raises:
    SkylabServerActionError: If any operation failed.
  """
  servers = infrastructure.servers
  server = _verify_server(servers, hostname, environment)

  if role:
    if not delete_role:
      _add_role(server, role)
    else:
      _delete_role(server, role)

  if status:
    _change_status(server, status)

  if note is not None:
    server.notes = note

  if attribute and value:
    _change_attribute(server, attribute, value)
  elif attribute and delete_attribute:
    _delete_attribute(server, attribute)

  return server
