# -*- coding: utf-8 -*-
# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Library module to read message from text and dump message to text.

This module collects some helper functions to load the skylab inventory text
files from disk into python protobuf objects, and to update the text files on
disk with corresponding protobuf objects.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from skylab_inventory import translation_utils
from skylab_inventory.protos import lab_pb2

from google.protobuf import text_format

import os


INVENTORY_DATA_DIR = 'data'
DATA_FILE_SERVER_DB = 'server_db.textpb'


def get_data_dir(inventory_repo_dir, data_subdir):
  """Get path to the data subdir.

  Args:
    inventory_repo_dir: Path to root dir of the internal inventory repo.
    data_subdir: The name of the data subdir, one of
                 translation_utils.DATA_SUBDIR.

  Returns:
    The path to the data dir (e.g. "~/skylab_inventory/data/prod/").
  """
  translation_utils.validate_data_subdir(data_subdir)
  return os.path.join(inventory_repo_dir, INVENTORY_DATA_DIR, data_subdir)


def load_text_to_proto_message(data_file_path, proto_message):
  """Load data from text file to proto_message object.

  Args:
    data_file_path: The path to the data file to load.
    proto_message: An instance of proto message.
  """
  with open(data_file_path) as f:
    text_format.Parse(f.read(), proto_message)


def dump_proto_message_to_text(proto_message, data_file_path):
  """Dump protocal message to a file on disk.

  Args:
    proto_message: Protocal message to be dumpped.
    data_file_path: Path to the file to dump message.
  """
  with open(data_file_path, 'w') as f:
    f.write(text_format.MessageToString(proto_message))


def load_infrastructure(data_dir, environments=None):
  """Loads the infrastructure information from the inventory.

  Args:
    data_dir: Path to the folder which contains server_db.textpb.
    environments: If environments is provided, it must be a set of environments
                  of servers to load, and it must be subset of
                  translation_utils.ENVIRONMENT_MAP.keys. Else, environments
                  is default to None, and this method loads all servers.

  Returns:
    An instance of lab_pb2.Infrastructure.
  """
  infrastructure = lab_pb2.Infrastructure()
  data_file_path = os.path.join(data_dir, DATA_FILE_SERVER_DB)
  load_text_to_proto_message(data_file_path, infrastructure)

  if environments is None:
    return infrastructure

  env_values = translation_utils.validate_environments(environments)
  infrastructure_result = lab_pb2.Infrastructure()

  for server in infrastructure.servers:
    if server.environment in env_values:
      infrastructure_result.servers.extend([server])

  return infrastructure_result


def sort_infrastructure(infrastructure_msg):
  """Sort lab_pb2.infrastructure instance.

  Args:
    infrastructure_msg: An instance of lab_pb2.Infrastructure.
  """
  for server in infrastructure_msg.servers:
    server.roles.sort()
    server.dut_uids.sort()

  infrastructure_msg.servers.sort(key=lambda x: x.hostname)


def dump_infrastructure(data_dir, infrastructure_msg):
  """Dump infrastructure proto_message to local text file.

  Args:
    data_dir: Path to the folder to dump server_db.textpb.
    infrastructure_msg: An instance of lab_pb2.Infrastructure.
  """
  data_file_path = os.path.join(data_dir, DATA_FILE_SERVER_DB)
  sort_infrastructure(infrastructure_msg)
  dump_proto_message_to_text(infrastructure_msg, data_file_path)