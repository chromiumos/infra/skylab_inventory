# Skylab inventory

[TOC]

## Updating inventory schema

Inventory schema is stored as protobuf definitions in the proto/ directory.
When you update these protobuf definitions, you must also update the generated
python libraries for them.

You need the protoc compiler for this. On gLinux:

```
sudo apt-get install protobuf-compiler
go get github.com/golang/protobuf/protoc-gen-go
```

Then, regenerate the libraries inside the chroot:

```
cros_sdk
(cr) cd ~/trunk/infra/skylab_inventory/protos
(cr) protoc --python_out=../venv/skylab_inventory/protos/ *.proto
(cr) protoc --go_out=../go/src/chromiumos/infra/skylab/inventory/protos/ *.proto
```
