# Skylab inventory Go library

[TOC]

## Development

All development should be done in the [chroot].

[chroot]: http://www.chromium.org/chromium-os/developer-guide/go-in-chromium-os

Make sure to set GOPATH according to the guide.

### Common tasks

To serve documentation:

    godoc -port 5000

To build binaries:

    make

To run unit tests:

    make check

To run all tests, including slow and/or flaky tests:

    make check_all

To check test coverage of a package:

    scripts/check_coverage lucifer/event
