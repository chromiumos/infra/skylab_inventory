/*
Package inventory contains tools for interacting with the Skylab
inventory.
*/
package inventory

import (
	"io/ioutil"
	"path/filepath"

	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"

	protos "chromiumos/infra/skylab/inventory/protos"
)

const serverFilename = "server_db.textpb"

func LoadServers(dataDir string) ([]*protos.Server, error) {
	b, err := ioutil.ReadFile(filepath.Join(dataDir, serverFilename))
	if err != nil {
		return nil, errors.Wrap(err, "failed to read server textproto file")
	}
	i := &protos.Infrastructure{}
	if err := proto.UnmarshalText(string(b), i); err != nil {
		return nil, errors.Wrap(err, "failed to umarshal server textproto")
	}
	return i.Servers, nil
}
