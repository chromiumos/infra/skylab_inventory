// Code generated by protoc-gen-go. DO NOT EDIT.
// source: connection.proto

package chrome_chromeos_infra_skylab_proto_inventory

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// NEXT TAG: 4
type ServoHostConnection struct {
	ServoHostId          *string  `protobuf:"bytes,1,req,name=servo_host_id,json=servoHostId" json:"servo_host_id,omitempty"`
	DutId                *string  `protobuf:"bytes,2,req,name=dut_id,json=dutId" json:"dut_id,omitempty"`
	ServoPort            *int32   `protobuf:"varint,3,req,name=servo_port,json=servoPort" json:"servo_port,omitempty"`
	ServoSerial          *string  `protobuf:"bytes,4,opt,name=servo_serial,json=servoSerial" json:"servo_serial,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ServoHostConnection) Reset()         { *m = ServoHostConnection{} }
func (m *ServoHostConnection) String() string { return proto.CompactTextString(m) }
func (*ServoHostConnection) ProtoMessage()    {}
func (*ServoHostConnection) Descriptor() ([]byte, []int) {
	return fileDescriptor_51baa40a1cc6b48b, []int{0}
}

func (m *ServoHostConnection) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ServoHostConnection.Unmarshal(m, b)
}
func (m *ServoHostConnection) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ServoHostConnection.Marshal(b, m, deterministic)
}
func (m *ServoHostConnection) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ServoHostConnection.Merge(m, src)
}
func (m *ServoHostConnection) XXX_Size() int {
	return xxx_messageInfo_ServoHostConnection.Size(m)
}
func (m *ServoHostConnection) XXX_DiscardUnknown() {
	xxx_messageInfo_ServoHostConnection.DiscardUnknown(m)
}

var xxx_messageInfo_ServoHostConnection proto.InternalMessageInfo

func (m *ServoHostConnection) GetServoHostId() string {
	if m != nil && m.ServoHostId != nil {
		return *m.ServoHostId
	}
	return ""
}

func (m *ServoHostConnection) GetDutId() string {
	if m != nil && m.DutId != nil {
		return *m.DutId
	}
	return ""
}

func (m *ServoHostConnection) GetServoPort() int32 {
	if m != nil && m.ServoPort != nil {
		return *m.ServoPort
	}
	return 0
}

func (m *ServoHostConnection) GetServoSerial() string {
	if m != nil && m.ServoSerial != nil {
		return *m.ServoSerial
	}
	return ""
}

// NEXT TAG: 3
type ChameleonConnection struct {
	Chameleon            *ChameleonDevice `protobuf:"bytes,1,req,name=chameleon" json:"chameleon,omitempty"`
	ControlledDevice     *Device          `protobuf:"bytes,2,req,name=controlled_device,json=controlledDevice" json:"controlled_device,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *ChameleonConnection) Reset()         { *m = ChameleonConnection{} }
func (m *ChameleonConnection) String() string { return proto.CompactTextString(m) }
func (*ChameleonConnection) ProtoMessage()    {}
func (*ChameleonConnection) Descriptor() ([]byte, []int) {
	return fileDescriptor_51baa40a1cc6b48b, []int{1}
}

func (m *ChameleonConnection) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ChameleonConnection.Unmarshal(m, b)
}
func (m *ChameleonConnection) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ChameleonConnection.Marshal(b, m, deterministic)
}
func (m *ChameleonConnection) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ChameleonConnection.Merge(m, src)
}
func (m *ChameleonConnection) XXX_Size() int {
	return xxx_messageInfo_ChameleonConnection.Size(m)
}
func (m *ChameleonConnection) XXX_DiscardUnknown() {
	xxx_messageInfo_ChameleonConnection.DiscardUnknown(m)
}

var xxx_messageInfo_ChameleonConnection proto.InternalMessageInfo

func (m *ChameleonConnection) GetChameleon() *ChameleonDevice {
	if m != nil {
		return m.Chameleon
	}
	return nil
}

func (m *ChameleonConnection) GetControlledDevice() *Device {
	if m != nil {
		return m.ControlledDevice
	}
	return nil
}

func init() {
	proto.RegisterType((*ServoHostConnection)(nil), "chrome.chromeos_infra.skylab.proto.inventory.ServoHostConnection")
	proto.RegisterType((*ChameleonConnection)(nil), "chrome.chromeos_infra.skylab.proto.inventory.ChameleonConnection")
}

func init() { proto.RegisterFile("connection.proto", fileDescriptor_51baa40a1cc6b48b) }

var fileDescriptor_51baa40a1cc6b48b = []byte{
	// 260 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0x90, 0x4f, 0x4b, 0x33, 0x31,
	0x10, 0x87, 0xd9, 0x7d, 0xdf, 0x0a, 0x3b, 0x5b, 0xa1, 0xa6, 0x08, 0x8b, 0x20, 0xac, 0x7b, 0xea,
	0x41, 0x72, 0x28, 0x5e, 0x3d, 0xd5, 0x83, 0xde, 0x64, 0x7b, 0xf4, 0xb0, 0xc4, 0x64, 0x64, 0x83,
	0x69, 0xa6, 0x24, 0xe9, 0x42, 0xbf, 0x86, 0xdf, 0xcd, 0xef, 0x23, 0x4d, 0xec, 0xd6, 0x6b, 0x4f,
	0x81, 0xe7, 0x37, 0x79, 0xe6, 0x0f, 0xcc, 0x24, 0x59, 0x8b, 0x32, 0x68, 0xb2, 0x7c, 0xeb, 0x28,
	0x10, 0xbb, 0x97, 0xbd, 0xa3, 0x0d, 0xf2, 0xf4, 0x90, 0xef, 0xb4, 0xfd, 0x70, 0x82, 0xfb, 0xcf,
	0xbd, 0x11, 0xef, 0xa9, 0x86, 0x6b, 0x3b, 0xa0, 0x0d, 0xe4, 0xf6, 0x37, 0x53, 0x85, 0x83, 0x96,
	0x98, 0x78, 0xf3, 0x95, 0xc1, 0x7c, 0x8d, 0x6e, 0xa0, 0x67, 0xf2, 0x61, 0x35, 0x9a, 0x59, 0x03,
	0x97, 0xfe, 0x80, 0xbb, 0x9e, 0x7c, 0xe8, 0xb4, 0xaa, 0xb2, 0x3a, 0x5f, 0x14, 0x6d, 0xe9, 0x8f,
	0xb5, 0x2f, 0x8a, 0x5d, 0xc3, 0x85, 0xda, 0xc5, 0x30, 0x8f, 0xe1, 0x44, 0xed, 0x0e, 0xf8, 0x16,
	0x20, 0x7d, 0xdd, 0x92, 0x0b, 0xd5, 0xbf, 0x3a, 0x5f, 0x4c, 0xda, 0x22, 0x92, 0x57, 0x72, 0x81,
	0xdd, 0xc1, 0x34, 0xc5, 0x1e, 0x9d, 0x16, 0xa6, 0xfa, 0x5f, 0x67, 0xa3, 0x78, 0x1d, 0x51, 0xf3,
	0x9d, 0xc1, 0x7c, 0xd5, 0x8b, 0x0d, 0x1a, 0x24, 0xfb, 0x67, 0xa8, 0x37, 0x28, 0xe4, 0x11, 0xc7,
	0x81, 0xca, 0xe5, 0x23, 0x3f, 0x67, 0x79, 0x3e, 0x5a, 0x9f, 0xe2, 0x11, 0xda, 0x93, 0x8f, 0x09,
	0xb8, 0x92, 0x64, 0x83, 0x23, 0x63, 0x50, 0x75, 0xe9, 0x48, 0x71, 0xb1, 0x72, 0xf9, 0x70, 0x5e,
	0x93, 0x5f, 0xf7, 0xec, 0xa4, 0x4b, 0xe4, 0x27, 0x00, 0x00, 0xff, 0xff, 0xaa, 0x13, 0x5c, 0x7a,
	0xbb, 0x01, 0x00, 0x00,
}
