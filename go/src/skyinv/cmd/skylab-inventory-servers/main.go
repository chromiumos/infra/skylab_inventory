/*
Program skylab-inventory-servers is an interactive client for
accessing inventory server data.
*/
package main

import (
	"flag"
	"fmt"
	"log"

	"chromiumos/infra/skylab/inventory"
)

func main() {
	a := parseArgs()
	s, err := inventory.LoadServers(a.dataDir)
	if err != nil {
		log.Fatalf("Error loading servers: %+v", err)
	}
	fmt.Printf("%+v", s)
}

type args struct {
	dataDir string
}

func parseArgs() *args {
	a := &args{}
	flag.StringVar(&a.dataDir, "datadir", "", "Path to inventory data directory")
	flag.Parse()
	return a
}
