package main

import (
	"go.chromium.org/luci/common/tsmon/field"
	"go.chromium.org/luci/common/tsmon/metric"
)

var (
	presenceMetric = metric.NewBool(
		"chromeos/skylab/inventory/servers/presence",
		"A boolean indicating whether a server is in the machines db.",
		nil,
		field.String("target_hostname"),
		field.String("target_data_center"),
		field.String("target_environment"))

	rolesMetric = metric.NewString(
		"chromeos/skylab/inventory/servers/roles",
		"A string indicating the role of a server in the machines db.",
		nil,
		field.String("target_hostname"),
		field.String("target_data_center"),
		field.String("target_environment"))

	ignoredMetric = metric.NewBool(
		"chromeos/skylab/inventory/servers/ignored",
		"A boolean, for servers ignored for test infra prod alerts.",
		nil,
		field.String("target_hostname"),
		field.String("target_data_center"),
		field.String("target_environment"))

	dutCountMetric = metric.NewInt(
		"chromeos/skylab/inventory/servers/dut_count",
		"An integer - the number of DUTs associated with the server.",
		nil,
		field.String("target_hostname"),
		field.String("target_data_center"),
		field.String("target_environment"))
)
