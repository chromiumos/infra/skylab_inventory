package main

import "testing"

func TestIsIgnoredHostname(t *testing.T) {
	t.Parallel()
	cases := []struct {
		h string
		v bool
	}{
		{"chromeos1-foo", true},
		{"chromeos3-foo", true},
		{"chromeos9-foo", true},
		{"chromeos15-foo", true},
		{"devserver-lv-foo", true},

		{"chromeos2-foo", false},
		{"chromeos2", false},
		{"chromeos", false},
		{"devserver-supported-foo", false},

		{"foo", false},
	}
	for _, c := range cases {
		t.Run(c.h, func(t *testing.T) {
			got := isIgnoredHostname(c.h)
			if got != c.v {
				t.Errorf("Got %#v, expected %#v", got, c.v)
			}
		})
	}
}
