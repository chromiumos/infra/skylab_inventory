// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Program skylab-inventory-mon reports the inventory as metrics for
// easy access in dashboards.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"go.chromium.org/luci/common/tsmon"
	"go.chromium.org/luci/common/tsmon/target"
	"golang.org/x/sys/unix"

	"chromiumos/infra/skylab/inventory"
	protos "chromiumos/infra/skylab/inventory/protos"
)

const programName = "skylab-inventory-mon"

func main() {
	if err := innerMain(); err != nil {
		log.Fatalf("Error: %+v", err)
	}
}

func innerMain() error {
	fl := tsmon.NewFlags()
	setDefaultTsmonFlags(&fl)
	a := parseArgs(&fl)
	ctx := context.Background()
	if a.dry {
		log.Printf("Skipping tsmon setup")
	} else {
		if err := tsmon.InitializeFromFlags(ctx, &fl); err != nil {
			return err
		}
		defer tsmon.Shutdown(ctx)
	}

	ctx, cancel := context.WithCancel(ctx)
	c := make(chan os.Signal, 1)
	defer close(c)
	signal.Notify(c, unix.SIGINT, unix.SIGTERM)
	defer signal.Stop(c)
	go callOnSignal(c, cancel)

	for ctx.Err() == nil {
		if err := tick(ctx, a); err != nil {
			log.Printf("Error during tick: %+v", err)
		}
		select {
		case <-time.After(time.Duration(a.interval) * time.Second):
		case <-ctx.Done():
		}
	}
	return nil
}

type args struct {
	dataDir  string
	dry      bool
	interval int
}

func parseArgs(fl *tsmon.Flags) *args {
	a := &args{}
	fl.Register(flag.CommandLine)
	flag.StringVar(&a.dataDir, "datadir", "", "Path to inventory data directory")
	flag.BoolVar(&a.dry, "dry-run", false, "Don't emit metrics")
	flag.IntVar(&a.interval, "interval", 600, "Interval between ticks in seconds (must be at least 60)")
	flag.Parse()
	return a
}

func setDefaultTsmonFlags(fl *tsmon.Flags) {
	fl.Flush = tsmon.FlushManual
	fl.Target.SetDefaultsFromHostname()
	fl.Target.TargetType = target.TaskType
	fl.Target.TaskServiceName = programName
	fl.Target.TaskJobName = programName
}

// callOnSignal calls a function when a signal is received or the
// channel is closed.  This function is blocking and should be called
// as a goroutine.  This function is intended for propagating
// cancellation.
func callOnSignal(c <-chan os.Signal, f func()) {
	<-c
	f()
}

func tick(ctx context.Context, a *args) error {
	servers, err := inventory.LoadServers(a.dataDir)
	if err != nil {
		return errors.Wrap(err, "failed to load servers")
	}
	for _, s := range servers {
		if err := sendServerMetrics(ctx, s); err != nil {
			msg := fmt.Sprintf("failed sending metrics for %s", s.GetHostname())
			return errors.Wrap(err, msg)
		}
	}
	if err := tsmon.Flush(ctx); err != nil {
		return errors.Wrap(err, "flush tsmon")
	}
	return nil
}

func sendServerMetrics(ctx context.Context, s *protos.Server) error {
	if s.Hostname == nil {
		return fmt.Errorf("no hostname")
	}
	f := s.GetHostname()
	parts := strings.SplitN(f, ".", 3)
	if len(parts) != 3 {
		parts = []string{f, "", ""}
	}
	h := parts[0]
	dc := parts[1]
	e := s.GetEnvironment().String()
	// Queen drones are not actual drones and thus are in
	// ENVIRONMENT_INVALID, but for the purpose of counting DUTs
	// assigned to them, infer the environment they are in.
	if isQueenDrone(f) {
		e = queenDroneEnvionment(f)
	}
	log.Printf("Sending metrics for %s in %s", f, e)
	presenceMetric.Set(ctx, true, h, dc, e)
	log.Printf("Sending ignored metric for %s: %t", f, isIgnored(s))
	ignoredMetric.Set(ctx, isIgnored(s), h, dc, e)
	if s.Roles == nil {
		return fmt.Errorf("%s Roles is nil", f)
	}
	rs := make([]string, len(s.Roles))
	for i, r := range s.Roles {
		rs[i] = r.String()
	}
	r := strings.Join(rs, ",")
	log.Printf("Sending roles for %s: %s", f, r)
	rolesMetric.Set(ctx, r, h, dc, e)
	dutCount := int64(len(s.DutUids))
	log.Printf("Sending DUT count for %s: %d", f, dutCount)
	dutCountMetric.Set(ctx, dutCount, h, dc, e)
	return nil
}

const queenDronePrefix = "drone-queen-"

func isQueenDrone(hostname string) bool {
	return strings.HasPrefix(hostname, queenDronePrefix)
}

func queenDroneEnvionment(hostname string) string {
	return hostname[len(queenDronePrefix):]
}

func isIgnored(s *protos.Server) bool {
	return isIgnoredHostname(s.GetHostname())
}

func isIgnoredHostname(hostname string) bool {
	if strings.HasPrefix(hostname, "chromeos") {
		return isHostInIgnoredChromeosLab(hostname)
	}
	if strings.HasPrefix(hostname, "devserver-lv-") {
		return true
	}
	return false
}

func isHostInIgnoredChromeosLab(hostname string) bool {
	// Testing for chromeos{1,3,5,...}-...
	s := hostname[len("chromeos"):]
	i := strings.Index(s, "-")
	if i == -1 {
		return false
	}
	n, err := strconv.Atoi(s[:i])
	if err != nil {
		return false
	}
	return n%2 == 1
}
