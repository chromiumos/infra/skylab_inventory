#!/bin/bash
# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -eu

function cleanup() {
    rm -rf "${py_tempdir}"
    rm -rf "${go_tempdir}"
}

readonly bindir="$(readlink -e -- "$(dirname -- "$0")")"
readonly topdir="$(dirname "${bindir}")"
readonly py_tempdir="$(mktemp -d)"
readonly py_bindings_dir="${topdir}/venv/skylab_inventory/protos"
readonly go_tempdir="$(mktemp -d)"
go_bindings_dir="${topdir}/go/src/chromiumos/infra/skylab/inventory/protos"
readonly go_bindings_dir
trap cleanup EXIT

source "${bindir}/setup_cipd"

if ! protoc --python_out="${py_tempdir}" -I "${topdir}/protos" \
  "${topdir}"/protos/*.proto &>/dev/null; then
  cat <<EOT
Failed to compile proto files. Please fix any compilations errors:
  protoc --python_out=venv/skylab_inventory/protos -I protos protos/*.proto
EOT
  exit 1
fi
# hand-maintained.
touch ${py_tempdir}/__init__.py

if ! diff -x \*.pyc -q "${py_tempdir}" "${py_bindings_dir}" &>/dev/null; then
  cat <<EOT
Found mismatch between proto definitions and generated bindings.
Please update bindings by running
  protoc --python_out=venv/skylab_inventory/protos -I protos protos/*.proto
EOT
  exit 1
fi

if ! protoc --go_out="${go_tempdir}" -I "${topdir}/protos" \
  "${topdir}"/protos/*.proto &>/dev/null; then
  cat <<EOT
Failed to compile proto files. Please fix any compilations errors:
  protoc --go_out=go/src/chromiumos/infra/skylab/inventory/protos \
    -I protos protos/*.proto
EOT
  exit 1
fi

if ! diff -x \*.pyc -q "${go_tempdir}" "${go_bindings_dir}" &>/dev/null; then
  cat <<EOT
Found mismatch between proto definitions and generated bindings.
Please update bindings by running
  protoc --go_out=go/src/chromiumos/infra/skylab/inventory/protos \
    -I protos protos/*.proto
EOT
  exit 1
fi
